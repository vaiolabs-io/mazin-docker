# Mazin-Docker


## Python based docker agent for remote management with minimal footprint

This project provides Rest-API service that can be deployed to self-contained container that will allow to manage automatic docker container update.
It should be used with docker-Engine and docker-Compose to update state of the service, that docker-compose is running.

In case you like the project, consider to contribute, fork the project or leave a star.

---

# Project Structure
```sh
Mazin-Docker
|-- .gitignore
|-- LICENSE
|-- README.md

```

---

# Contribution

Please check issues in case you wish to contribute under GPLv2 license guidelines
 
# Todo

- add ssl support
- add elastic support
- combine hooks into classes
- manage docker compose with lib and not service
- compact it into container
- add elasticsearch for dynamic lookup.




---

Copyright (C) 2022  VaioLabs LTD.
